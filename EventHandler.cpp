/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of CContactExample for AQQ                                *
 *                                                                             *
 * CContactExample plugin is free software: you can redistribute it and/or     *
 * modify it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * CContactExample is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "EventHandler.h"
#include "SDK\PluginLink.h"
#include "PluginAPI.h"
#include "SDK\AQQ.h"
#include "CContactExample.h"


CEventHandler::CEventHandler()
{
	CPluginLink::instance()->GetLink().HookEvent(AQQ_CONTACTS_LISTREADY,
		ListReady);
}

CEventHandler::~CEventHandler()
{
	CPluginLink::instance()->GetLink().UnhookEvent(ListReady);
}

INT_PTR __stdcall CEventHandler::ListReady(WPARAM wParam,LPARAM lParam)
{
	MyContact->SetStatus(CONTACT_ONLINE, "Description");
	return 0;
}
