/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of CContactExample for AQQ                                *
 *                                                                             *
 * CContactExample plugin is free software: you can redistribute it and/or     *
 * modify it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * CContactExample is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include <vcl.h>
#include <windows.h>

// SDK includes
#include "SDK\PluginLink.h"
#include "SDK\PluginAPI.h"
#include "SDK\Contact.h"
#include "SDK\AQQ.h"

#include "EventHandler.h"


CContact* MyContact;
CEventHandler* EventHandler;

UnicodeString AVATAR_URL =
	"http://static.gossamer-threads.com/forum/images/users/18071-user_icon-1.png";

TPluginInfo PluginInfo;

int WINAPI DllEntryPoint(HINSTANCE hinst,unsigned long reason,void* lpReserved)
{
	return 1;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Load(PPluginLink Link)
{
	CPluginLink::instance()->SetLink(*Link);

	MyContact = new CContact();
	// Contact
	MyContact->JID = "example@contact";
	MyContact->Nick = "Example";
	MyContact->Resource = "";
	MyContact->Groups = "ExampleGroup";
	MyContact->Temporary = false;
	MyContact->FromPlugin = true;
	MyContact->UserIDx = 0;
	MyContact->Subscription = SUB_BOTH;
	MyContact->Chat = false;
	if (AQQ::Functions::IsListReady())
  		MyContact->SetStatus(CONTACT_ONLINE, "Description");
	//Contact info
	MyContact->ContactInfo->FirstName = "John";
	MyContact->ContactInfo->LastName = "Smith";
	MyContact->ContactInfo->CellPhone = "123456789";
	MyContact->ContactInfo->Phone = "0";
	MyContact->ContactInfo->Mail = "example@sdk.plugin";

	MyContact->Create();
	MyContact->Avatar->SetWebAvatar(AVATAR_URL);

	EventHandler = new CEventHandler();
	return 0;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Unload()
{
	MyContact->Delete();
	delete MyContact;
	delete EventHandler;
	return 0;
}

extern "C" __declspec(dllexport) PPluginInfo __stdcall AQQPluginInfo
	(DWORD AQQVersion)
{
	PluginInfo.cbSize = sizeof(TPluginInfo);
	PluginInfo.ShortName = L"CContactExample";
	PluginInfo.Version = PLUGIN_MAKE_VERSION(1,0,0,0);
	PluginInfo.Description =
		L"Przyk�adowa wtyczka tworz�ca kontakt za pomoc� SDK.";
	PluginInfo.Author = L"Rafa� Babiarz (sauler)";
	PluginInfo.AuthorMail = L"sauler1995@gmail.com";
	PluginInfo.Copyright = L"sauler";
	PluginInfo.Homepage = L"";
	PluginInfo.Flag = 0;
	PluginInfo.ReplaceDefaultModule = 0;

	return &PluginInfo;
}
