# CContactExample
CContactExample jest wtyczką do komunikatora [AQQ](http://www.aqq.eu/pl.php). Jest to wtyczka, pokazująca jak tworzyć kontakty za pomocą [SDK dla komunikatora AQQ](https://bitbucket.org/sauler/aqq-sdk).

### Wymagania
Do skompilowania wtyczki potrzebne jest:

* Embarcadero RAD Studio XE10 Seattle
* [SDK dla komunikatora AQQ](https://bitbucket.org/sauler/aqq-sdk)
* Opcjonalnie [UPX](http://upx.sourceforge.net/) dla zmniejszenia rozmiaru pliku wynikowego

### Błędy
Znalezione błędy wtyczki należy zgłaszać pisząc bezpośrednio do autora wtyczki (preferowany kontakt poprzez Jabber)

### Kontakt z autorem
Autorem wtyczki CContactExample jest Rafał Babiarz. Możesz skontaktować się z nim drogą mailową (sauler1995@gmail.com) lub poprzez Jabber (sauler@jix.im).

### Licencja
Wtyczka CContactExample objęta jest licencją [GNU General Public License 3](http://www.gnu.org/copyleft/gpl.html).

~~~~
CContactExample
Copyright © 2016  Rafał Babiarz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
~~~~